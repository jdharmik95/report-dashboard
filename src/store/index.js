const initializeStore = function(storeId = null){
    return {
        state: {
        storeId,
        metricSelected: null,
        metrics: [],
        records: [],
        xAxis: null
        },
        mutations: {
            setxAxis(state, key) {
                this.replaceState({
                    ...state,
                    xAxis: key
                });
            },
            setRecords (state, records = []) {
                this.replaceState({
                    ...state,
                    records
                });
            },
            setMetrics (state, metrics = []){
                this.replaceState({
                    ...state,
                    metrics
                });
            },
            setMetricSelected (state, key){
                this.replaceState({
                    ...state,
                    metricSelected: key
                });
            }
        },
        actions: {
            initializeState ({commit}, reportData) { // created this action to initialize state, this can be extended to set the data received from the json response dynamically.
                let metrics = Object.keys(reportData.records[0]); // using first record to find the list of metrics available
                metrics = metrics.filter((key)=>{
                    return key==reportData.xaxis?false:true // filtered xaxis field from the list of metrics
                });
                commit('setRecords',reportData.records); // setting the list of records received from report json
                commit('setMetricSelected',metrics[0]); // setting first metric available as initial selected metric
                commit('setMetrics',metrics); // list of metrics 
                commit('setxAxis',reportData.xaxis);
            },
            setMetricSelected({commit},key){
                commit('setMetricSelected',key);
            }
        }
    }
};
export default initializeStore;