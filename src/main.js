import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import initializeStore from './store';
const reportData = require('./mockData/report.json'); // loading the mocked report json data
const reportData1 = require('./mockData/report1.json'); 

Vue.use(Vuex);
const appState0 = initializeStore(0);
const appState1 = initializeStore(1);

let store = new Vuex.Store(appState0);
let store1 = new Vuex.Store(appState1);


Vue.config.productionTip = false

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  store
})

new Vue({
  el: '#app1',
  components: { App },
  template: '<App/>',
  store: store1
})


store.dispatch('initializeState', reportData); // loading report data into store from report.json
store1.dispatch('initializeState', reportData1);

// we can reuse the same action if we have an api call to be made, on success of fetch call we intialize the state with the response received.